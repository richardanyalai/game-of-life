CC			:= gcc

CFLAGS		:= -Wall -Wextra -Werror
LDFLAGS		:= -lSDL2 -lm
DIMENSION	?=

run: gol
	@./gol $(DIMENSION)

gol: gol.c
	@$(CC) $(CFLAGS) -o gol gol.c $(LDFLAGS)

clean:
	@rm -rf gol
