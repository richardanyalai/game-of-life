#include <SDL2/SDL.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#define for_x for (int x = 0; x < width; ++x)
#define for_y for (int y = 0; y < height; ++y)
#define for_xy for_x for_y
#define for_yx for_y for_x
#define get_pos(x, y) (x + y * width)

int width, height, mouse_x, mouse_y;
bool running, paused, *map;

SDL_Window *window;
SDL_Renderer *renderer;
SDL_Event event;

SDL_Rect cell;

void init_map()
{
	map = malloc(sizeof(bool) * width * height);

	srand(time(0));

	for_yx map[get_pos(x, y)] = rand() % 2 == 0;
}

void init(int width, int height)
{
	int flags = 0;

	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		printf("Subsystem initialized...\n");
		window = SDL_CreateWindow("Game of Life", SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED, width * 30, height * 30, flags);

		if (window)
			printf("Window created...\n");

		renderer = SDL_CreateRenderer(window, -1, 0);

		if (renderer)
		{
			printf("Renderer created...\n");
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
			SDL_RenderSetScale(renderer, 1, 1);

			running = true;
			paused = true;

			init_map();
		}

		running = true;
	}
	else
		running = false;
}

void render()
{
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);

	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

	for_yx
	{
		int pos = get_pos(x, y);

		cell.w = 29;
		cell.h = cell.w;

		cell.x = x * 30 + 1;
		cell.y = y * 30 + 1;

		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

		if (map[pos])
			SDL_SetRenderDrawColor(renderer, 10, 255, 150, 255);

		SDL_RenderFillRect(renderer, &cell);
	}

	SDL_RenderPresent(renderer);
}

int count_alive(int i, int j)
{
	int a = 0;

	for (int x = i - 1; x <= i + 1; ++x)
		for (int y = j - 1; y <= j + 1; ++y)
		{
			if (x == i && y == j)
				continue;

			if (y < height && x < width && x >= 0 && y >= 0)
				a += map[get_pos(x, y)];
		}

	return a;
}

void update()
{
	bool *newgen = malloc(sizeof(bool) * width * height);

	for (int i = 0; i < width * height; ++i)
		newgen[i] = 0;

	for_yx
	{
		int cur_pos = get_pos(x, y);

		bool cur = map[cur_pos];

		int alive_neighbours = count_alive(x, y);

		if (cur && alive_neighbours < 2)
			newgen[cur_pos] = 0;
		else if (cur && alive_neighbours > 3)
			newgen[cur_pos] = 0;
		else if (!cur && alive_neighbours == 3)
			newgen[cur_pos] = 1;
		else
			newgen[cur_pos] = cur;
	}

	free(map);
	map = newgen;
}

void cleanup()
{
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);

	SDL_Quit();
	printf("Game cleaned...");

	free(map);
}

void switch_cell(int x, int y)
{
	int pos = get_pos(x / 30, y / 30);

	map[pos] = !map[pos];

	cell.w = 29;
	cell.h = cell.w;

	cell.x = x * 30 + 1;
	cell.y = y * 30 + 1;

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

	if (map[pos])
		SDL_SetRenderDrawColor(renderer, 10, 255, 150, 255);

	SDL_RenderFillRect(renderer, &cell);
	SDL_RenderPresent(renderer);

	render();
}

void handleEvents(SDL_Event *event)
{
	switch (event->type)
	{
		case SDL_KEYUP:
			switch (event->key.keysym.sym)
			{
				case SDLK_SPACE:
					paused = !paused;
					break;

				case SDLK_a:
					paused = true;
					free(map);
					map = malloc(sizeof(bool) * width * height);
					srand(time(0));
					for_yx map[get_pos(x, y)] = 1;
					render();
					break;

				case SDLK_c:
					paused = true;
					free(map);
					map = malloc(sizeof(bool) * width * height);
					srand(time(0));
					for_yx map[get_pos(x, y)] = 0;
					render();
					break;

				case SDLK_r:
					paused = true;
					free(map);
					init_map();
					render();
					break;

				case SDLK_q:
					exit(0);
					break;
			}
			break;

		case SDL_MOUSEBUTTONDOWN:
			SDL_GetMouseState(&mouse_x, &mouse_y);
			switch_cell(mouse_x, mouse_y);
			break;

		case SDL_QUIT:
			running = false;
			break;
	}
}

int main(int argc, char **argv)
{
	if (argc > 1)
	{
		width = atoi(argv[1]);
		height = width;
	}

	if (argc > 2)
		height = atoi(argv[2]);

	if (argc == 1)
	{
		width = 15;
		height = 15;
	}

	init(width, height);
	render();

	while (running)
	{
		while (SDL_PollEvent(&event))
			handleEvents(&event);

		if (!paused)
		{
			update();
			render();
			usleep(200000);
		}
	}

	cleanup();
}
