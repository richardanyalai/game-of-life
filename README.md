# Game of Life

A simple implementation of Conway's Game of Life.

___

## Table of contents

1. [Table of contents](#table-of-contents)
1. [About this project](#about-this-project)
1. [Screenshots](#in-game-footage)
1. [Let's play](#let's-play)
   1. [Setting up the environment](#setting-up-the-environment)
   1. [Building and running](#building-and-running)
1. [Useful links](#useful-links)

___

## About this project

This project is a minimal implementation of [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) using the [SDL framework](https://www.libsdl.org/), written in C.

The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician [John Horton Conway](https://en.wikipedia.org/wiki/John_Horton_Conway) in 1970. It is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. One interacts with the Game of Life by creating an initial configuration and observing how it evolves. It is Turing complete and can simulate a universal constructor or any other Turing machine.

Here is a little cheatsheet for the keys used in the game:

| Key   | Function      |
|-------|---------------|
| space | start / pause |
| r     | randomize     |
| c     | clear map     |
| a     | fill map      |
| q / e | exit          |

___

## In-game footage

![Screenshot](screenshots/screen.png)

___

## Let's play

In order to be able to play the game, you need to compile it from source.

### Setting up the environment

If you use a Debian-based GNU+Linux distribution, make sure to install the necessary packages by executing the following command:

````bash
sudo apt-get install libsdl2-dev
````

If you are an Arch or Manjaro user, use the following command instead:

````bash
sudo pacman -S sdl2
````

After installing he necessary packages, you have to clone the git repository:

````bash
git clone https://gitlab.com/richardanyalai/game-of-life
````

### Building and running

To build the game you can issue this command explicitly:

````bash
gcc -w -fcompare-debug-second -o gol gol.c -lSDL2 -lm
````

Then run it by typing in:

````bash
./gol width height
````

Or use ````make DIMENSION="width height"```` to build and run.
where `width` and `height` are the dimensions of the game window (in cells, not pixels!).

For simplicity you can use the `make` command itself.

___

## Useful links

[Wikipedia/Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)  
[Wikipedia/Cellular automaton](https://en.wikipedia.org/wiki/Cellular_automaton)  
[Wikipedia/Turing machine](https://en.wikipedia.org/wiki/Turing_machine)  
